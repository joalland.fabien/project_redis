var redis = require("redis");
var prompt = require('prompt');
var sub = redis.createClient();
var pub = redis.createClient();
var message;
var pseudo;

prompt.start();
function pseudoPrompt() {
    prompt.get(['pseudo'], function (err, result) {
        if (err) {
            return onErr(err);
        }
        pseudo = result.pseudo;
        getMessage();
    });
}
function getMessage() {
    prompt.get(['messages'], function (err, result) {
        if (err) {
            return onErr(err);
        }
        message = result.messages;
        pub.publish("Chat Channel", pseudo + " : " + message);
        getMessage();
    });
}

sub.on("message", function (channel, message) {
    console.log(message);
});

sub.subscribe("Chat Channel");
pseudoPrompt();